from django.core.management.base import BaseCommand, CommandError
from ca.ca import CA
import logging

log = logging.getLogger(__name__)

class Command(BaseCommand):
    help = "Initialize the CA for signing client certificates"

    def handle(self, **opts):
        ca = CA()
        ca.init()
