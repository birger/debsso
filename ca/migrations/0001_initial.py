# -*- coding: utf-8 -*-


from django.db import models, migrations
from django.conf import settings
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Certificate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('comment', models.TextField(help_text='optional comment to identify this enrollment in the enrollment log (eg. hostname)', blank=True)),
                ('validity', models.IntegerField(default=365, help_text='Validity of the certificate in days', validators=[django.core.validators.MinValueValidator(1), django.core.validators.MaxValueValidator(365)])),
                ('der', models.BinaryField(help_text='DER-encoded certificate', null=True)),
                ('generated', models.DateTimeField(help_text='When the certificate was generated', null=True)),
                ('serial', models.BigIntegerField(help_text='Serial number of the certificate', null=True)),
                ('revoked', models.DateTimeField(help_text='When the certificate has been revoked', null=True)),
                ('user', models.ForeignKey(help_text='User corresponding to this certificate', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
