from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.db import models
from django.conf import settings
from django.core import validators
import datetime
import ssl

class Certificate(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             help_text=_("User corresponding to this certificate"))

    comment = models.TextField(
        null=False,
        blank=True,
        help_text=_("optional comment to identify this enrollment in the enrollment log (eg. hostname)"))

    validity = models.IntegerField(
        default=365,
        help_text=_("Validity of the certificate in days (max 365)"),
        validators=[validators.MinValueValidator(1),
                    validators.MaxValueValidator(365)])

    der = models.BinaryField(null=True,
                             help_text=_("DER-encoded certificate"))

    generated = models.DateTimeField(null=True,
                             help_text=_("When the certificate was generated"))

    serial = models.TextField(null=True, db_index=True,
                             help_text=_("Serial number of the certificate (text version)"))

    revoked = models.DateTimeField(null=True,
                             help_text=_("When the certificate has been revoked"))

    @property
    def serial_formatted(self):
        return self.serial

    @property
    def expiration_date(self):
        return self.generated + datetime.timedelta(days=self.validity)

    @property
    def expired(self):
        return self.expiration_date <= timezone.now()

    @property
    def pem(self):
        return ssl.DER_cert_to_PEM_cert(self.der)

    @classmethod
    def get_userid_field_name(cls):
        """
        Get the name of the AUTH_USER_MODEL field that will be used to identify
        a user in the certificate.

        This can be controlled using the SPKAC_USERID_FIELD setting, and it
        defaults to "id".
        """
        return getattr(settings, "SPKAC_USERID_FIELD", "id")
