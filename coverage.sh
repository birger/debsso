#!/bin/sh
CMD=python3-coverage
MODULES=${@:-"ca deblayout debsso debssolayout spkac sso"}
eatmydata $CMD run ./manage.py test $MODULES &&
    $CMD html &&
    sensible-browser htmlcov/index.html
