from django.test import TestCase, Client, override_settings
from django.core.urlresolvers import reverse
from django.contrib.auth import get_user_model
from django.utils.timezone import now
from ca.models import Certificate
from ca.tests import test_ca
import shutil
import subprocess
import contextlib
import tempfile
import os


@override_settings(TEST_USER="test@debian.org")
class TestCerts(TestCase):
    @contextlib.contextmanager
    def tempdir(self):
        try:
            tmpdir = tempfile.mkdtemp()
            yield tmpdir
        finally:
            shutil.rmtree(tmpdir)

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user = get_user_model().objects.create_user(email="test@debian.org", first_name="test", last_name="test", provider="debian.org")

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        cls.user.delete()

    def make_spkac_key(self, challenge):
        with self.tempdir() as root:
            testfile = os.path.join(root, "test.key")
            with open("/dev/null", "wb") as devnull:
                rsakey = subprocess.check_output(["openssl", "genrsa", "-out", testfile, "2048"], stderr=devnull)
                spkac = subprocess.check_output(["openssl", "spkac", "-key", testfile, "-challenge", challenge])
                return spkac.split(b"=", 1)[1].decode("utf-8")

    def make_csr_key(self, user):
        with test_ca() as ca:
            userid = getattr(user, Certificate.get_userid_field_name())
            subject = "/CN={}/O={}".format(userid, ca.organization)

        with self.tempdir() as root:
            testfile = os.path.join(root, "test.key")
            with open("/dev/null", "wb") as devnull:
                rsakey = subprocess.check_output(["openssl", "genrsa", "-out", testfile, "2048"], stderr=devnull)
                return subprocess.check_output(["openssl", "req", "-new", "-sha256", "-subj", subject, "-key", testfile, "-batch"], universal_newlines=True)

    def test_status(self):
        response = self.client.get(reverse("spkac_status", args=["debian"]))
        self.assertContains(response, "Get new certificate")

    def test_enroll(self):
        response = self.client.get(reverse("spkac_enroll", args=["debian"]))
        self.assertContains(response, "Enrollment type:")

        challenge = response.context["challenge"]
        spkac = self.make_spkac_key(challenge)

        response = self.client.post(reverse("spkac_enroll_manually", args=["debian"]), data={ "key": spkac, "challenge": challenge, "validity": 365 })
        self.assertContains(response, b"Debian SSO client certificate")
        self.assertNotIn("Content-Type", "application/x-x509-user-cert")
        self.assertNotIn("Content-Disposition", response)

    def test_enroll_manually(self):
        response = self.client.get(reverse("spkac_enroll_manually", args=["debian"]))
        self.assertContains(response, "Run these commands to generate an RSA key")

        challenge = response.context["challenge"]
        spkac = self.make_spkac_key(challenge)

        response = self.client.post(reverse("spkac_enroll_manually", args=["debian"]), data={ "key": spkac, "challenge": challenge, "validity": 365, "manual": 1 })
        self.assertContains(response, b"-----BEGIN CERTIFICATE-----")
        self.assertNotIn("Content-Type", "application/octet-stream")
        self.assertEqual(response["Content-Disposition"], 'attachment; filename="test@debian.org.crt"')

    def test_enroll_csr(self):
        response = self.client.get(reverse("spkac_enroll_csr", args=["debian"]))
        self.assertContains(response, "Run these commands to generate an RSA key")
        self.assertEqual(response.context["subject"], "'/CN=test@debian.org/O=Debian SSO client certificate'")

        csr = self.make_csr_key(self.user)

        response = self.client.post(reverse("spkac_enroll_csr", args=["debian"]), data={
            "csr": csr, "validity": 365
        })
        self.assertContains(response, b"-----BEGIN CERTIFICATE-----")
        self.assertNotIn("Content-Type", "application/octet-stream")
        self.assertEqual(response["Content-Disposition"], 'attachment; filename="test@debian.org.crt"')

    def test_revoke(self):
        response = self.client.get(reverse("ca_crl"))
        self.assertContains(response, "-----BEGIN X509 CRL-----")
        crl1 = response.content

        # Make a key
        response = self.client.get(reverse("spkac_enroll_manually", args=["debian"]))
        challenge = response.context["challenge"]
        spkac = self.make_spkac_key(challenge)
        response = self.client.post(reverse("spkac_enroll_manually", args=["debian"]), data={ "key": spkac, "challenge": challenge, "validity": 365, "manual": 1 })
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "-----BEGIN CERTIFICATE-----")
        cert = Certificate.objects.get()
        response = self.client.post(reverse("spkac_revoke", args=["debian", cert.serial]))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response["Location"], "/debian/certs/")

        response = self.client.get(reverse("ca_crl"))
        self.assertContains(response, "-----BEGIN X509 CRL-----")
        crl2 = response.content

        self.assertNotEqual(crl1, crl2)
