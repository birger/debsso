from django.conf import settings
import re

re_domain_path = re.compile(r"^/(debian|alioth)/")

class SSOAuthMiddleware(object):
    def process_request(self, request):
        # Allow to override the current user via settings for tests
        remote_user = getattr(settings, "TEST_USER", None)
        if remote_user is not None and re_domain_path.match(request.path):
            request.META["REMOTE_USER"] = remote_user
            return

        # Get user from SSO certificates
        cert_user = request.META.get("SSL_CLIENT_S_DN_CN", None)
        if cert_user is not None:
            request.META["REMOTE_USER"] = cert_user
            return

        # Get user and domain from Apache
        remote_user = request.META.get("REMOTE_USER", None)
        if remote_user is None:
            return

        domain = request.META.get("SSO_DOMAIN", None)
        if domain is not None:
            request.META["REMOTE_USER"] = remote_user + "@" + domain
        else:
            del request.META["REMOTE_USER"]
